import { factory } from "./libs/ConfigLog4j";
import { ErrorCallback, WorkerService } from "./libs/worker";
import * as IdentityLib from './libs/identity'
import { ApiPromise } from "@polkadot/api";
import { KeyringPair } from '@polkadot/keyring/types';
import type { EventRecord, AccountId, Hash } from '@polkadot/types/interfaces';
import { UnsubscribePromise, VoidFn } from "@polkadot/api/types";

const log = factory.getLogger("debug.registrarservice");

export class RegistrarService extends WorkerService {

    RegistrarId? : Number;
    Account : KeyringPair;
    Unsubscribe? : VoidFn;

    getUniqueID(): string {
        return "REGISTRAR_SERVICE"
    }

    constructor(account : KeyringPair) {
        super()
        this.Account = account;
    }

    async customStart() : Promise<Boolean> {
        // make sure this oracle is a valid registrar
        this.RegistrarId = await IdentityLib.getRegistrarId(this.api!, this.Account!.address);
        if(this.RegistrarId < 0) {
            log.error("account used with oracle isn't a registrar");
            return false;
        }
        // subscribe to events, which is triggered for every block with events
        this.Unsubscribe = await this.api!.query.system.events((events) => {
            // Loop through the Vec<EventRecord>
            events.forEach((record : EventRecord) => {
                const { event } = record;
                if(event.section === "identity" && event.method === "JudgementRequested") {
                    let account = event.data[0].toHuman()!.toString()
                    let registrarID = Number(event.data[1]);
                    if(registrarID === this.RegistrarId)
                        this.processRequest(account);
                    
                }
            });
        });
        // fetch old events
        let records = await IdentityLib.getFilteredLogs(this.api!, "identity", "JudgementRequested")
        records.forEach(record => {
            const { event } = record;
            let account = event.data[0].toHuman()!.toString();
            let registrarID = Number(event.data[1]);
            if(registrarID === this.RegistrarId)
                this.processRequest(account);
        })
        return true
    }

    async customStop() : Promise<Boolean> {
        if(this.Unsubscribe)
            await this.Unsubscribe!();
        return true;
    }

    /**
     * Provides a judgement for the requested account, which is always "Reasonable"
     * @param account Account waiting for judgement
     */
    async processRequest(account : string) {
        let result = await this.api!.tx.identity.provideJudgement(this.RegistrarId!.valueOf(), account, "Reasonable");
        let hash = await result.signAndSend(this.Account!);
    }
}