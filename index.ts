import express from 'express';
import OracleZero from './oracle'
const app = express();
const PORT = 8000;
app.get('/', (req, res) => res.send('Express + TypeScript Server'));
app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
  let oracle = new OracleZero("ws://127.0.0.1:9944", "//Alice")
  oracle.start()
});