import { ApiPromise, WsProvider } from "@polkadot/api";
import {factory} from "./ConfigLog4j";
const log = factory.getLogger("debug.worker");

export enum EWorkerState {
    INIT = 0,
    RUNNING,
    STOPPED,
    ERROR
}

export enum EServiceState {
    INIT = 0,
    READY,
    RUNNING,
    STOPPED,
    ERROR
}

interface IServiceManager {
    getService : <T extends WorkerService>(serviceUniqueId : string) => T | undefined;
    hasService : (serviceUniqueId : string) => Boolean;
    registerService : (service : WorkerService) => Boolean;
}

export type ErrorCallback = (msg : string, critical : boolean) => void;

// tslint:disable-next-line: no-any
type Constructor<T> = new (...args: any[]) => T;

export function ofType<TElements, TFilter extends TElements>(array: TElements[], filterType: Constructor<TFilter>): TFilter[] {
    return <TFilter[]>array.filter(e => e instanceof filterType);
}

export abstract class WorkerService {

    protected state : EServiceState;
    protected errorCallback? : ErrorCallback; //never call this in start
    protected api? : ApiPromise;
    protected manager? : IServiceManager;

    getState() : EServiceState { return this.state; }
    abstract getUniqueID() : string;

    /**
     */
    constructor() {
        this.state = EServiceState.INIT;
    }

    register(manager : IServiceManager) {
        this.manager = manager;
        this.changeState(EServiceState.READY);
    }

    /**
     * Called by the worker
     *@returns True if startup was completed
     */
    async start(onErrorCallback : ErrorCallback, api : ApiPromise) : Promise<Boolean> {
        log.info("starting worker service: " + this.getUniqueID());
        this.api = api;
        this.errorCallback = onErrorCallback;

        if(this.state === EServiceState.INIT ) {
            log.error("state must be READY")
            return false;
        }

        if(this.state === EServiceState.RUNNING )
            return true;
        
        if(await this.customStart()) {
            this.changeState(EServiceState.RUNNING);
            return true;
        } else
            return false;
    }

    /**
     * Called by the worker
     *@returns True if shutdown is completed
     */
    async stop() : Promise<Boolean> {
        log.info("stopping worker service: " + this.getUniqueID());
        if(await this.customStop()) {
            this.changeState(EServiceState.STOPPED);
            return true;
        } else
            return false;
    }

    /**
     * Implement this for your service custom start steps
     */
    abstract customStart() : Promise<Boolean>;
    /**
     * Implement this for your service custom stop steps
     */
    abstract customStop() : Promise<Boolean>;

    /**
     * Call this from the service to change state
     * @param newState New state to transition to
     */
    changeState(newState : EServiceState) {
        log.info("service " + this.getUniqueID() + " transitioned state (" + this.state + "=>" + newState + ")");
        this.state = newState;
    }
}

export default class ZeroWorker implements IServiceManager {
    api :       ApiPromise;
    state :     EWorkerState;
    services :  Array<WorkerService>;
    ws_url :    string;

    constructor(ws_url : string) {
        // init default api
        this.api = new ApiPromise();
        this.state = EWorkerState.INIT;
        this.services = new Array<WorkerService>();
        this.ws_url = ws_url;
    }

    /**
     * Connects the worker to the node
     * @param ws_url WS url and port to the node
     */
    async start() : Promise<Boolean> {
        log.info("worker setup");
        try {
            const wsProvider = new WsProvider(this.ws_url);
            log.info("connecting api");
            this.api = await ApiPromise.create({ provider: wsProvider });
        } catch(error) {
            log.error("api connection error: " + error);
            this.state = EWorkerState.ERROR;
            return false;
        }
        
        if(this.api.isConnected)
            log.info("api connected");
        else {
            log.error("api connection error")
            return false;
        }
        log.info("starting services")
        if(await this.startServices())
            log.info("services started")
        return true;
    }

    /**
     * Stops the services and breaks connection
     */
    async stop() : Promise<Boolean> {
        log.info("stopping services");
        if(await this.stopServices())
        log.info("services stopped")
        log.info("disconnecting api");
        await this.api.disconnect()
        log.info("api disconnected");
        return true;
    }

    /**
     * Register a service in the worker
     * @param service A service in INIT state
     */
    registerService(service : WorkerService) : Boolean {
        log.info("registering service:" + service.getUniqueID());
        if(this.hasService(service.getUniqueID())) {
            log.error("service already present: " + service.getUniqueID());
            return false;
        }
        if(service.getState() !== EServiceState.INIT) {
            log.error("service must be in INIT state, current state: " + service.getState());
            return false;
        }
        if(this.state !== EWorkerState.INIT) {
            log.error("services can be registered only if worker is in INIT state, current state: " + this.state)
            return false;
        }
        service.register(this);
        this.services.push(service);
        log.info("service registered");
        return true;
    }

    /**
     * @returns True if the service is present in the worker
     * @param serviceUniqueId Service unique name
     */
    hasService(serviceUniqueId : string) : Boolean {
        return this.getService<WorkerService>(serviceUniqueId) != undefined;
    }
    

    /**
     * Finds and returns a service
     * @param serviceUniqueId Service unique name
     * @returns The service object, undefined if not found, it is converted into the given type
     */
    getService<T extends WorkerService>(serviceUniqueId : string) : T | undefined {
       return <T>this.services.find(service => {
           return service.getUniqueID() === serviceUniqueId;
       });
    }

    /**
     * Called by a service in case of error or warning
     * Never call during start phase!
     * @param msg Error message
     * @param critical True if the worker should be stopped
     */
    private onServiceError(msg : string, critical : boolean) {
        if(!critical)
            log.warn("service warning: " + msg);
        else {
            log.error("service error: " + msg);
            log.info("stopping the worker")
            this.stop()
        }
    }

    /**
     * Start all the services
     * Wait and make sure they have the correct state
     */
    private async startServices() : Promise<Boolean> {
        let promises = new Array<Promise<Boolean>>()
        this.services.forEach(service => {
            promises.push(service.start(this.onServiceError, this.api))
        })
        await Promise.all(promises)
        let error = false
        this.services.forEach(service => {
           if(service.getState() !== EServiceState.RUNNING)
           {
               log.error("service " + service.getUniqueID() + " failed to start");
               error = true;
           }
        })
        if(error) {
            log.error("worker failed to start");
            this.stop();
        }
        return !error;
    }

    /**
     * Stop all the services
     * Wait and make sure they have the correct state
     */
    private async stopServices() : Promise<Boolean> {
        let promises = new Array<Promise<Boolean>>()
        this.services.forEach(service => {
            promises.push(service.stop())
        })
        await Promise.all(promises)
        let error = false
        this.services.forEach(service => {
           if(service.getState() !== EServiceState.STOPPED)
           {
               log.error("service " + service.getUniqueID() + " failed to stop");
               error = true;
           }
        })
        if(error) {
            log.error("worker failed to stop")
        }
        return error;
    }
}