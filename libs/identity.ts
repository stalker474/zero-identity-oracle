import { ApiPromise, Keyring } from '@polkadot/api';
import { factory } from "./ConfigLog4j";
import type { EventRecord, AccountId, Hash } from '@polkadot/types/interfaces';
import type { RegistrarInfo } from '@polkadot/types/interfaces/identity';
import type { Vec, u16, u32, u64, Option } from '@polkadot/types';
import { Address } from 'cluster';

const log = factory.getLogger("debug.library");

/**
 * Fetches all past event logs and filters them by section
 * @param api connected polkadotjs api
 * @param section pallet from which the event was fired : system, identity, etc
 */
export async function getFilteredLogs(api : ApiPromise, section : string, method? : string, beginHash? : Hash, endHash? : Hash) : Promise<Array<EventRecord>> {
    // range consists of begin and end block hash, end block hash being optional
    let from = beginHash? beginHash : api.genesisHash
    let range : [string | Hash | Uint8Array, string | Hash | Uint8Array | undefined] | [string | Hash | Uint8Array] = [from]
    if(endHash)
        range.push(endHash)
    let logs = await api.query.system.events.range(range);
    let logsArray = Array<EventRecord>();
    logs.forEach(pair => {
        let events = pair[1].filter(record => { return record.event.section === section && (!method || record.event.method === method)});
        events.forEach(event => {
            logsArray.push(event);
        })
    })
    return logsArray
}

/**
 * Validates if an address is a registrar
 * @param api connected polkadotjs api
 * @param section pallet from which the event was fired : system, identity, etc
 */
export async function getRegistrarId(api : ApiPromise, account : string) : Promise<Number> {
    let registrars = await api.query.identity.registrars();
    //this is ugly but if I use indexOf typescript fails typing?
    for(let i = 0; i < registrars.length; i++) {
        // @ts-ignore
        let registrar : RegistrarInfo = registrars[i].value
        if(registrar.account.toHuman() === account)
            return i
    }
    return -1;
}