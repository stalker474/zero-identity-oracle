import {LoggerFactory, LoggerFactoryOptions, LFService, LogGroupRule, LogLevel} from "typescript-logging";

const options = new LoggerFactoryOptions()
.addLogGroupRule(new LogGroupRule(new RegExp("debug.+"), LogLevel.Debug))
.addLogGroupRule(new LogGroupRule(new RegExp("info.+"), LogLevel.Info));

// Create a named loggerfactory and pass in the options and export the factory.
// Named is since version 0.2.+ (it's recommended for future usage)
export const factory = LFService.createNamedLoggerFactory("LoggerFactory", options);