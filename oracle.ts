import * as IdentityLib from './libs/identity'
import {factory} from "./libs/ConfigLog4j";
import { ApiPromise, WsProvider, Keyring } from '@polkadot/api';
import type { EventRecord, AccountId, Hash } from '@polkadot/types/interfaces';
import type { RegistrarInfo } from '@polkadot/types/interfaces/identity';
import type { Vec, u16, u32, u64, Option } from '@polkadot/types';
import { Address } from 'cluster';
import ZeroWorker from './libs/worker';
import { KeyringPair } from '@polkadot/keyring/types';
import { cryptoWaitReady } from '@polkadot/util-crypto'
import { RegistrarService } from './services';

const keyring = new Keyring({ type: 'sr25519' });

const log = factory.getLogger("debug.oracle");

export default class OracleZero extends ZeroWorker {

    RegistrarId : Number = -1;
    account? : KeyringPair;
    keyringPath : string;
    
    /**
     * Init the oracle
     * @param ws_url Url of the node to connect to
     * @param keyringPath Path to the keyring file with the account with registrar authorizations
     */
    constructor(ws_url : string, keyringPath : string) {
        super(ws_url);
        this.keyringPath = keyringPath;
    }

    async start() : Promise<Boolean> {
        log.info("oracle starting");
        //this needs to return before we can use keyring
        await cryptoWaitReady();
        try {
            this.account = keyring.addFromUri(this.keyringPath, { name: 'Registrar account' });
            log.info("keyring account " + this.keyringPath + " loaded succesfully, address: " + this.account!.address)
        } catch(error) {
            log.error("failed to load keyring account " + this.keyringPath + ": " + error);
            return false;
        }
        this.registerService(new RegistrarService(this.account!));

        return super.start();
    }

    async stop() : Promise<Boolean> {
        if(await super.stop()) {
            log.info("oracle stopped");
            return true;
        }
        else {
            log.error("oracle failed to stop");
            return false;
        }
    }
}